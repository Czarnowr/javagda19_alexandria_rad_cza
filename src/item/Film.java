package item;

import java.util.Date;

public class Film extends Item{

    private String director;

    public Film(String title, String description, String creator, Date creationYear, int edition, int numAvailable, int numTotal, String director) {
        super(title, description, creator, creationYear, edition, numAvailable, numTotal);
        this.director = director;
    }
}
