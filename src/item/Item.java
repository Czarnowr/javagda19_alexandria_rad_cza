package item;

import java.util.Date;

abstract class Item {

    protected String title;
    protected String description;
    protected String creator;
    Date creationYear;
    int edition;
    protected int numAvailable;
    protected int numTotal;

    public Item(String title, String description, String creator, Date creationYear, int edition, int numAvailable, int numTotal) {
        this.title = title;
        this.description = description;
        this.creator = creator;
        this.creationYear = creationYear;
        this.edition = edition;
        this.numAvailable = numAvailable;
        this.numTotal = numTotal;
    }
}
