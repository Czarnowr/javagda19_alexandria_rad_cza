package item;

import java.util.Date;

public class Vinyl extends Item {

    int totalLength;
    int type;

    public Vinyl(String title, String description, String creator, Date creationYear, int edition, int numAvailable, int numTotal, int totalLength, int type) {
        super(title, description, creator, creationYear, edition, numAvailable, numTotal);
        this.totalLength = totalLength;
        this.type = type;
    }
}
