package item;

import java.util.Date;

public class Book extends Item {


    private int totalPages;
    private double weight;
    private BookCategory category;

    public Book(String title, String description, String creator, Date creationYear, int edition, int numAvailable, int numTotal, int totalPages, double weight, BookCategory category) {
        super(title, description, creator, creationYear, edition, numAvailable, numTotal);
        this.totalPages = totalPages;
        this.weight = weight;
        this.category = category;
    }
}
