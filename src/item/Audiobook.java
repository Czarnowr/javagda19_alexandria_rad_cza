package item;

import java.util.Date;

public class Audiobook extends Item{

    private int length;
    private String fileType;

    public Audiobook(String title, String description, String creator, Date creationYear, int edition, int numAvailable, int numTotal, int length, String fileType) {
        super(title, description, creator, creationYear, edition, numAvailable, numTotal);
        this.length = length;
        this.fileType = fileType;
    }
}
