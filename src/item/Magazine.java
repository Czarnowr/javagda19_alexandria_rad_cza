package item;

import java.util.Date;

public class Magazine extends Item {

    public Magazine(String title, String description, String creator, Date creationYear, int edition, int numAvailable, int numTotal) {
        super(title, description, creator, creationYear, edition, numAvailable, numTotal);
    }
}
