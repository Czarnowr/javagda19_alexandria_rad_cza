package user;

public class User {

    private String name;
    private String surname;
    private String email;
    private String phone;
    private String password;
    private UserRole role;
    private String userId;
    private double moneyOwed;

    public User(String name, String surname, String email, String phone, String password, UserRole role, String userId, double moneyOwed) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.role = role;
        this.userId = userId;
        this.moneyOwed = moneyOwed;
    }
}
