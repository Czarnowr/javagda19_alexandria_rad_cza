package user;

public enum UserRole {

    ADMIN,
    CUSTOMER,
    PUBLIC;
}
