import item.Book;
import item.Vinyl;
import user.User;
import user.UserRole;

public class AlexandriaLibrary {

    public User createCustomer(String name, String surname, String email, String phone, String password, String userId, double moneyOwed){
        return new User(name, surname, email, phone, password, UserRole.CUSTOMER, userId, moneyOwed);
    }

    public User createAdmin(String name, String surname, String email, String phone, String password, String userId, double moneyOwed){
        return new User(name, surname, email, phone, password, UserRole.ADMIN, userId, moneyOwed);
    }

}


